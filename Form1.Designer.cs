﻿namespace CatRenamer {
    partial class Form1 {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.bRename = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.labelNewNames = new System.Windows.Forms.Label();
            this.tbNewNames = new System.Windows.Forms.TextBox();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.labelSize = new System.Windows.Forms.Label();
            this.tbSizeToIncrease = new System.Windows.Forms.TextBox();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.rbDate = new System.Windows.Forms.RadioButton();
            this.rbText = new System.Windows.Forms.RadioButton();
            this.cbResize = new System.Windows.Forms.CheckBox();
            this.tbDirectory = new System.Windows.Forms.TextBox();
            this.bOpenFolder = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // bRename
            // 
            this.bRename.Enabled = false;
            this.bRename.Location = new System.Drawing.Point(138, 172);
            this.bRename.Name = "bRename";
            this.bRename.Size = new System.Drawing.Size(100, 25);
            this.bRename.TabIndex = 0;
            this.bRename.Text = "Rename All";
            this.bRename.UseVisualStyleBackColor = true;
            this.bRename.Click += new System.EventHandler(this.bRename_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(49, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Select directory:";
            // 
            // labelNewNames
            // 
            this.labelNewNames.AutoSize = true;
            this.labelNewNames.Location = new System.Drawing.Point(12, 120);
            this.labelNewNames.Name = "labelNewNames";
            this.labelNewNames.Size = new System.Drawing.Size(120, 13);
            this.labelNewNames.TabIndex = 2;
            this.labelNewNames.Text = "Enter name of new files:";
            this.labelNewNames.Visible = false;
            // 
            // tbNewNames
            // 
            this.tbNewNames.Location = new System.Drawing.Point(138, 117);
            this.tbNewNames.Name = "tbNewNames";
            this.tbNewNames.Size = new System.Drawing.Size(330, 20);
            this.tbNewNames.TabIndex = 3;
            this.tbNewNames.Visible = false;
            // 
            // labelSize
            // 
            this.labelSize.AutoSize = true;
            this.labelSize.Location = new System.Drawing.Point(25, 146);
            this.labelSize.Name = "labelSize";
            this.labelSize.Size = new System.Drawing.Size(107, 13);
            this.labelSize.TabIndex = 6;
            this.labelSize.Text = "Size to increase in %:";
            this.labelSize.Visible = false;
            // 
            // tbSizeToIncrease
            // 
            this.tbSizeToIncrease.Location = new System.Drawing.Point(138, 143);
            this.tbSizeToIncrease.Name = "tbSizeToIncrease";
            this.tbSizeToIncrease.Size = new System.Drawing.Size(100, 20);
            this.tbSizeToIncrease.TabIndex = 8;
            this.tbSizeToIncrease.Text = "20";
            this.tbSizeToIncrease.Visible = false;
            // 
            // progressBar
            // 
            this.progressBar.Location = new System.Drawing.Point(15, 203);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(456, 23);
            this.progressBar.TabIndex = 10;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(348, 146);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(123, 51);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 11;
            this.pictureBox1.TabStop = false;
            // 
            // rbDate
            // 
            this.rbDate.AutoSize = true;
            this.rbDate.Location = new System.Drawing.Point(138, 93);
            this.rbDate.Name = "rbDate";
            this.rbDate.Size = new System.Drawing.Size(103, 17);
            this.rbDate.TabIndex = 12;
            this.rbDate.TabStop = true;
            this.rbDate.Text = "Rename as date";
            this.rbDate.UseVisualStyleBackColor = true;
            this.rbDate.CheckedChanged += new System.EventHandler(this.rbDate_CheckedChanged);
            // 
            // rbText
            // 
            this.rbText.AutoSize = true;
            this.rbText.Location = new System.Drawing.Point(247, 93);
            this.rbText.Name = "rbText";
            this.rbText.Size = new System.Drawing.Size(99, 17);
            this.rbText.TabIndex = 13;
            this.rbText.TabStop = true;
            this.rbText.Text = "Rename as text";
            this.rbText.UseVisualStyleBackColor = true;
            this.rbText.CheckedChanged += new System.EventHandler(this.rbText_CheckedChanged);
            // 
            // cbResize
            // 
            this.cbResize.AutoSize = true;
            this.cbResize.Location = new System.Drawing.Point(352, 94);
            this.cbResize.Name = "cbResize";
            this.cbResize.Size = new System.Drawing.Size(100, 17);
            this.cbResize.TabIndex = 14;
            this.cbResize.Text = "Resize images?";
            this.cbResize.UseVisualStyleBackColor = true;
            this.cbResize.CheckedChanged += new System.EventHandler(this.cbResize_CheckedChanged);
            // 
            // tbDirectory
            // 
            this.tbDirectory.Location = new System.Drawing.Point(138, 10);
            this.tbDirectory.Name = "tbDirectory";
            this.tbDirectory.Size = new System.Drawing.Size(333, 20);
            this.tbDirectory.TabIndex = 4;
            // 
            // bOpenFolder
            // 
            this.bOpenFolder.Location = new System.Drawing.Point(138, 37);
            this.bOpenFolder.Name = "bOpenFolder";
            this.bOpenFolder.Size = new System.Drawing.Size(100, 25);
            this.bOpenFolder.TabIndex = 15;
            this.bOpenFolder.Text = "Open Folder...";
            this.bOpenFolder.UseVisualStyleBackColor = true;
            this.bOpenFolder.Click += new System.EventHandler(this.bOpenFolder_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(480, 238);
            this.Controls.Add(this.bOpenFolder);
            this.Controls.Add(this.cbResize);
            this.Controls.Add(this.rbText);
            this.Controls.Add(this.rbDate);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.progressBar);
            this.Controls.Add(this.tbSizeToIncrease);
            this.Controls.Add(this.labelSize);
            this.Controls.Add(this.tbDirectory);
            this.Controls.Add(this.tbNewNames);
            this.Controls.Add(this.labelNewNames);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.bRename);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.Text = "Cat Renamer";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bRename;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelNewNames;
        private System.Windows.Forms.TextBox tbNewNames;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.Label labelSize;
        private System.Windows.Forms.TextBox tbSizeToIncrease;
        private System.Windows.Forms.ProgressBar progressBar;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.RadioButton rbDate;
        private System.Windows.Forms.RadioButton rbText;
        private System.Windows.Forms.CheckBox cbResize;
        private System.Windows.Forms.TextBox tbDirectory;
        private System.Windows.Forms.Button bOpenFolder;
    }
}

