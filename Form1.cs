﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Threading;
using System.Windows.Forms;

namespace CatRenamer {

    public partial class Form1 : Form {

        private string directory;
        private string[] fileEntries;
        private string newNames;
        private string warningMsg;
        private int percentageSize;
        private const string RESIZED_DIRECTORY = "\\RESIZED";

        public Form1() {
            InitializeComponent();
        }

        private void bRename_Click(object sender, EventArgs e) {
            if (checkInputData()) {
                renameFiles();
            } else {
                MessageBox.Show(warningMsg);
            }
        }

        private bool checkInputData() {
            directory = tbDirectory.Text;
            if (Directory.Exists(directory)) {
                if (!cbResize.Checked) {
                    if (rbText.Checked && tbNewNames.Text == "") {
                        warningMsg = "Enter all data";
                        return false;
                    } else {
                        return true;
                    }
                } else {
                    if (cbResize.Checked && tbSizeToIncrease.Text != "") {
                        return true;
                    } else {
                        warningMsg = "Enter valid size to increase";
                        return false;
                    }
                }
            } else {
                warningMsg = "Directory does not exist";
                return false;
            }
        }

        private void renameFiles() {
            fileEntries = Directory.GetFiles(directory);
            progressBar.Maximum = fileEntries.Length;
            if (rbDate.Checked) {
                renameWithDate();
            } else if (rbText.Checked) {
                renameWithText(fileEntries);
            }
            if (cbResize.Checked) {
                // needs checks
                // resizeImages(extension, newFileNamePath, newFileName);
                percentageSize = Int16.Parse(tbSizeToIncrease.Text);
                Directory.CreateDirectory(directory + RESIZED_DIRECTORY);
            }
            progressBar.Refresh();
            progressBar.Value = 0;
        }

        private void renameWithText(string[] fileEntries) {
            int index = 1;
            int filesNumber = fileEntries.Length;
            newNames = tbNewNames.Text;
            foreach (string oldFileNamePath in fileEntries) {
                string extension = Path.GetExtension(oldFileNamePath);
                string newFileName = newNames + "-" + index + extension;
                string newFileNamePath = directory + "\\" + newFileName;
                File.Move(oldFileNamePath, newFileNamePath);
                index++;
                Thread.Sleep(50);
                updateProgressBar(filesNumber, index);
            }
        }

        private void renameWithDate() {
            foreach (var file in fileEntries) {
                Image image = new Bitmap(file);
                PropertyItem test = image.GetPropertyItem(0x132);
            }
        }

        private void resizeImages(string extension, string newFileNamePath, string newFileName) {
            if (!extension.Equals(".gif")) {
                using (Image newImage = Image.FromFile(newFileNamePath)) {
                    int newWidth = (int)(newImage.Width * (1 + ((double)percentageSize / 100)));
                    int newHeight = (int)(newImage.Height * (1 + ((double)percentageSize / 100)));
                    using (Image newImage2 = ResizeImage(newFileNamePath, newWidth, newHeight)) {
                        newImage2.Save(directory + RESIZED_DIRECTORY + "\\" + newFileName, ImageFormat.Jpeg);
                    }
                }
            } else {
                // only move
                File.Move(newFileNamePath, directory + RESIZED_DIRECTORY + "\\" + newFileName);
            }
        }

        private void updateProgressBar(int filesNumber, int i) {
            progressBar.Refresh();
            progressBar.CreateGraphics().DrawString(i + " / " + filesNumber,
                new Font("Arial", (float)10.0, FontStyle.Regular),
                Brushes.Black,
                new PointF(progressBar.Width / 2 - 10, progressBar.Height / 2 - 7));
            progressBar.Increment(1);
        }

        public static Bitmap ResizeImage(String path, int width, int height) {
            Image image = Image.FromFile(path);
            var destRect = new Rectangle(0, 0, width, height);
            var destImage = new Bitmap(width, height);
            destImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);
            using (var graphics = Graphics.FromImage(destImage)) {
                graphics.CompositingMode = CompositingMode.SourceCopy;
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.SmoothingMode = SmoothingMode.HighQuality;
                graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;
                using (var wrapMode = new ImageAttributes()) {
                    wrapMode.SetWrapMode(WrapMode.TileFlipXY);
                    graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
                }
            }
            //image.Dispose();
            return destImage;
        }

        private void rbText_CheckedChanged(object sender, EventArgs e) {
            if (rbText.Checked) {
                labelNewNames.Visible = true;
                tbNewNames.Visible = true;
                bRename.Enabled = true;
            }
        }

        private void rbDate_CheckedChanged(object sender, EventArgs e) {
            if (rbDate.Checked) {
                labelNewNames.Visible = false;
                tbNewNames.Visible = false;
                bRename.Enabled = true;
            }
        }

        private void cbResize_CheckedChanged(object sender, EventArgs e) {
            if (cbResize.Checked) {
                tbSizeToIncrease.Visible = true;
                labelSize.Visible = true;
            } else {
                tbSizeToIncrease.Visible = false;
                labelSize.Visible = false;
            }
        }

        private void bOpenFolder_Click(object sender, EventArgs e) {
            // open file dialog
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            DialogResult result = fbd.ShowDialog();
            if (fbd.SelectedPath.Length > 0) {
                tbDirectory.Text = fbd.SelectedPath;
            }
        }


    }
}